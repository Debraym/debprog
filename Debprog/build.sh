#!/bin/sh

lb config -d bookworm \
          --debian-installer live \
          --debian-installer-distribution bookworm \
          --debian-installer-gui true \
          --archive-areas "main contrib non-free-firmware non-free" \
          --debootstrap-options "--variant=minbase" \
          --iso-publisher 'Order of OpenSource Revolutionaries; Debraym Tea and D-Prog Xeonovich' \
          --iso-volume debprog \
          --clean \
          --color \
          --image-name Debprog GNU/Linux 12.1 \

sudo lb build
