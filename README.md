# Debprog
My custom Debian based distribution: https://drive.google.com/drive/folders/1CI3d2WbtVZczoyL_uPAdeel1alYxd5Wg?usp=sharing

<h3>Build:</h3>

**First you need to install live-build package:**

``sudo apt install live-build``

**Download config files:**

``git clone https://gitlab.com/Debraym/debprog``

**Go to desired directory:**

``cd Debprog``

**Run live-build scipt:**

``sh ./build.sh``

Resuting ISO file will be placed to current directory.